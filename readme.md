## Projeto de documentação de Software

<img src="https://img.shields.io/badge/codename-ursa%20menor-a">
<img src="https://img.shields.io/badge/versão-1.0.0-teal">
<img src="https://img.shields.io/badge/revisão-0-red">
<img src="https://img.shields.io/badge/licença-free-blue">

## Sobre 

Este é um modelo de readme que pretendo adotar para os projetos com os quais estou trabalhando no momento.

### Tabela de conteúdo

- Título 
- Justifica do projeto 
- Objetivos 
- Colaboradores 
- Premissas, Limitações e restrições
- Custos 

## Justificativa do projeto 

Nesta sessão entram detalhes da problemática a ser atacada, o que motivou o desenvolvimento e que situações ou contextos este pode vir a estar inserido. 

## Objetivos 

Aqui cabe uma lista com os objetivos principais da aplicação:

- [] Planejar uma estrutura
- [] Listar os passos necessários para...
- [] Manter os registros atualizados...

## Colaboradores

Este é um espaço mais amplo onde pode-se listar quem esteve envolvido no projeto e suas expertises:

- [Lucas Gabriel G. dos Santos](t.me/lucgbrl) - Engenheiro de Computação 

## Restrições

### Premissas

"O projeto deverá ser desenvolvido respeitando a arquitetura RESTful com um cliente ReactJS + React Native e uma API desenvolvida utilizando NodeJS..."


### Restrições 

Para o projeto deve-se focar no uso de:

- 1vCPU + 1GB RAM + 25GB SSD 
- Ubuntu Server 
- NginX 
- NodeJS v14.10.0 
- ReactJS...

## Custos 

Os custos podem ser definidos anualmente ou trimestralmente, gsrantindo que os módulos fundamentais para o funcionamento e organização do projeto permaneçam conhecidos. 
